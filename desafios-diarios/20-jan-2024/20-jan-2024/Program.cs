﻿// See https://aka.ms/new-console-template for more information

namespace desafiosDiarios;
class Program
{
    static void Main(string[] args)
    {
        var isValid = ValidateArgs(args);
        
        if(!isValid)
            return;
        
        Console.WriteLine("Você inseriu os seguintes valores: \nDepósito inicial: {0}\nMeses: {1} \nTaxa de Juros: {2}. \nProcessando...", args[0], args[1], args[2]);
        
        var initialDepoist = decimal.Parse(args[0]);
        var months = int.Parse(args[1]);
        var interestRate = double.Parse(args[2]) / 100;

        var finalValue = CalculaInvestimento(initialDepoist, months, interestRate);
        
        Console.WriteLine("Com um investimento inicial de {0}, após {1} meses com uma taxa de juros anual de {2}, você terá um total de:\n{3}", 
            initialDepoist.ToString("C"), months.ToString(), interestRate.ToString("P"), finalValue.ToString("C"));
    }

    private static bool ValidateArgs(string[] target)
    {
        if (target.Length != 3)
        {
            Console.WriteLine("A quantidade de argumentos fornecidas é diferente da esperada!");
            return false;
        }
        
        if(!decimal.TryParse(target[0], out var deposit))
        {
            Console.WriteLine("Não foi possível converter o valor do depósito inicial");
            return false;
        }

        if (deposit == Decimal.Zero)
        {
            Console.WriteLine("O valor inicial é 0, logo seu resultado também será 0.");
            return true;
        }

        if (!int.TryParse(target[1], out var meses))
        {
            Console.WriteLine("Não foi possível converter a quantidade de meses!");
            return false;
        }

        if (meses < 1)
        {
            Console.WriteLine("A quantidade de meses deve ser maior que zero!");
            return false;
        }

        if (!double.TryParse(target[2], out var juros))
        {
            Console.WriteLine("Não foi possível converter a taxa de juros!");
            return false;
        }

        if (juros < 0)
        {
            Console.WriteLine("Sua taxa de juros é negativa! Você irá PERDER dinheiro!");
        }

        return true;
    }

    private static decimal CalculaInvestimento(decimal initialValue, int months, double interestRate)
    {
        double monthlyRate = (double)months / 12;
        double monthlyInterestRate = Math.Pow(1 + interestRate, monthlyRate);
        return initialValue * (decimal)monthlyInterestRate;
    }
}