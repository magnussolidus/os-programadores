# Desafio proposto por Marcelo Pinheiro

Moçada, desafio relampago para hoje: Façam um programa que aceite dois 3 paramêtros via linha de comando. 
- O primeiro parâmetro é uma quantia qualquer, 
- o segundo uma quantidade de meses 
- e o terceiro o juros anual aplicado sobre investimentos. 

O programa vai pegar estes três parâmetros e exibir o valor futuro do investimento levando em consideração os juros e a quantidade de meses em que o dinheiro fiará investido. 

E ai? Quem vai fazer?

Data da proposição: 20 de Janeiro de 2024