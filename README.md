# Sobre este Repositório

Este é o meu repositório pessoal para adicionar as contribuições que fiz dos desafios diários que foram propostos no 
[grupo do telegram Os Programadores](https://t.me/osprogramadores).
Se você não conhece, pode visitá-los [neste site](https://osprogramadores.com/).

## Sobre Os Programadores

Os Programadores é uma iniciativa para auxiliar quem deseja começar na área de desenvolvimento. 
Eles possuem uma comunidade ativa, desafios para você aprender enquanto faz e um podcast onde entrevista pessoas da área para compartilharem sua experiência.
Normalmente, eles indicam iniciar [por este artigo](https://osprogramadores.com/blog/2019/03/12/por-onde-comecar/).

## Licença
Este projeto utiliza a licença [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Status do Projeto
Este projeto é feito em meu tempo pessoal. Cada contribuição ao desafio diario proposto no grupo que eu participar será adicionada ao repo no seguinte formato:

- Dentro da Pasta de Desafios Diários, será adicionada uma nova pasta contendo a data do desafio proposto.
- Dentro desta pasta, estará o código utilizado com a solução do desafio proposto.
- O desafio proposto deve estar descrito em um arquivo MD dentro desta pasta.
